import java.util.*
import kotlin.system.exitProcess

//https://gitlab.com/diego.arteaga.7e6/colecionesuf5.git

//Colecciones
val scanner = Scanner(System.`in`)
val reset = 	"\u001b[39m"
val black = "\u001b[30m"
val red ="\u001b[31m"
val green = "\u001b[32m"
val yellow = "\u001b[33m"
val blue = "\u001b[34m"
val magenta = "\u001b[35m"
val cyan = "\u001b[36m"

fun main() {
    roadSigns()
}

fun roadSigns() {
//1- RoadSigns
    val kmCarretera = 10000
    val carteles = mutableSetOf<String>()

    println("¿Cuantos carteles quieres colocar?")
    val nCarteles = scanner.nextInt()

    println("Introduce el contenido de tus $nCarteles carteles uno por uno.")
    for (i in 0..nCarteles) {
        val cotenidoCartel = scanner.nextLine()
        carteles.add(cotenidoCartel)
    }
    var cartelMetro = kmCarretera / nCarteles
    println("Hay un cartel cada $cartelMetro metros.")
    val cartelesEnCarretera = mutableMapOf<Int, String>()
    val infocartel = mutableListOf<String>()
    carteles.forEach { elemento ->
        infocartel.add(elemento)
    }
    for (i in 0 .. nCarteles) {
        var metros = cartelMetro
        var contenido = cartelesEnCarretera.put(metros, infocartel[i])
        metros += metros
    }
    println("Ahora puedes consultar si en X metro hay un cartel o no.")
    var exit = false
    do {
        println("Introduce el metro sobre el que quieres hacer la consulta o introduce 0 para salir del programa:")
        val metroAConsultar = scanner.nextInt()
        if (metroAConsultar == 0) exit = true
        if (cartelesEnCarretera.contains(metroAConsultar)) {
            println("$green${cartelesEnCarretera[metroAConsultar]}$reset")
        } else {
            println("${red}No hay cartel$reset")
        }
    } while (!exit)
}


